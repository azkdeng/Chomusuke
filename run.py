from bot import Chomusuke

if __name__ == '__main__':
    bot = Chomusuke()
    try:
        bot.run(bot.config['token'])
    except Exception as e:
        print('Exception: {}\n{}'.format(type(e).__name__, e))
        raise