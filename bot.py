from discord.ext import commands
from pprint import pprint
import os
import discord
import asyncio
import json
import logging
import datetime

LOG_FMT = '%(asctime)s | %(levelname)s | %(module)s | %(funcName)s | %(message)s'
LOG_DFMT = '[%m/%d/%Y %H:%M:%S]'

cogs = [
    'cogs.test',
]

class Chomusuke(commands.Bot):
    def __init__(self, *args, **kwargs):
        self._load_config()
        self._setup_logger()
        super().__init__(command_prefix=self.config['prefix'], description=self.config['description'], owner_id=self.config['owner'], *args, **kwargs)

    def _load_config(self):
        self.config = json.load(open('config.json'))
        os.environ['PATH'] += ":/usr/local/Cellar/ffmpeg/3.3/bin"

    def _setup_logger(self):
        self.logger = logging.getLogger('Chomusuke')
        self.logger.setLevel(logging.INFO)
        handler = logging.FileHandler(filename='logs/chomusuke.log', encoding='utf-8')
        handler.setFormatter(logging.Formatter(fmt=LOG_FMT, datefmt=LOG_DFMT))
        self.logger.addHandler(handler)

    async def on_ready(self):
        self.logger.info('\n--------------\nStarting up...\n--------------')
        for cog in cogs:
            try:
                self.load_extension(cog)
                self.logger.info('Cog loaded -- {}'.format(cog))
            except Exception as e:
                self.logger.debug('Failed to load cog -- {}\n{}: {}'.format(cog, type(e).__name__, e))

        self.logger.info('Logging in as: {}'.format(self.user))
        self.logger.info('\n----------------------\nCogs loaded, bot ready\n----------------------')

        self.start_time = datetime.datetime.utcnow()
        await self.change_presence(game=discord.Game(name='Testing'))

    async def on_message(self, message):
        if message.author.bot:
            return
        try:
            await self.process_commands(message)
        except Exception as e:
            self.logger.debug('Exception processing command --\n{}: {}'.format(type(e).__name__, e))